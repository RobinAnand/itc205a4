package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;

class BookingTest {
	
	Guest guest;
	String name="rb";
	String address="abcd";
	int phoneNumber=1234;
	
	CreditCard card;
	
	CreditCardType type=CreditCardType.VISA;
	int number=1;
	int ccv=1;
	
	Room room;
	int roomId=1;
	RoomType roomtype=RoomType.SINGLE;
	
	Date arrivalDate;
	
	int stayLength;
	int occupantNumber;
	
	
	long confirmationNumber;
	Booking booking;
	
	ServiceType serviceType;
	double cost;
	
	ServiceCharge serviceCharge;
	
	List<ServiceCharge> charges=new ArrayList<>();
	SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");
	
	

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		guest=new Guest(name,address,phoneNumber);
		room= new Room(roomId,roomtype);
		card=new CreditCard(type,number,ccv);
		
		format=new SimpleDateFormat("dd-MM-yyyy");
		arrivalDate=format.parse("11-12-2001");
		stayLength=1;
		
		
		occupantNumber=1;
		
		booking=new Booking(guest, room, arrivalDate, stayLength, occupantNumber, card);
		confirmationNumber=11122001101L;
		cost=15;
		serviceType=ServiceType.RESTAURANT;
		
		
		
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAddServiceCharge() {
		
		
        booking.addServiceCharge(serviceType, cost);
		
		charges=booking.getCharges();
		
		double servCharge=charges.get(0).getCost();
		assertEquals(15, servCharge);
		
	}

}
