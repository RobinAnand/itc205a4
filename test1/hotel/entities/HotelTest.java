package hotel.entities;


import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class HotelTest {
	Hotel hotel =new Hotel();
	@Mock Booking mockBooking;
	int roomNumber;
	
	
	
	 
	

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		
		
		
		roomNumber=1;
		
		
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCheckOut() {
		
	
		
		hotel.activeBookingsByRoomId.put(roomNumber,mockBooking);
		hotel.checkout(roomNumber);
		
		assertEquals(null,hotel.activeBookingsByRoomId.get(roomNumber));
		
		
		
		
		
		
	}

}
